package com.example.nicolascatalan.calculadescuentos;

import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

public class Metodos {

    public static String formateaValor(float valor)
    {
        DecimalFormat formatea = new DecimalFormat("###,###.##");

        String valorFormateado = formatea.format(valor);
        return valorFormateado;
    }

    public static String comprobarValorEntero(float descuentoIngresado) {
        if (descuentoIngresado % 1 == 0)
            return Constantes.OPERADOR_ENTERO;
        else
            return Constantes.OPERADOR_DECIMAL;
    }


}
