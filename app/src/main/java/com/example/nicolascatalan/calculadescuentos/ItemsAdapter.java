package com.example.nicolascatalan.calculadescuentos;

import android.content.ClipData;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ViewHolder> {

    //Arreglo para contener la cantidad de items a verificar descuento
    private List<Items> listItems;
    //Contexto para conseguir los recursos de la app, como por ejemplo algun Drawable
    private Context context;
    private OnItemClickListener eventoListener;

    public ItemsAdapter(List<Items> items, OnItemClickListener listener) {
        this.listItems = items;
        this.eventoListener = listener;
    }

    //Métodos minimos en el adaptador para poder utilizar el RecyclerView
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //En este metodo se debe inflar la vista para poder mostrar la vista item_list
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        //Desde aqui conseguimos el contexto del padre
        this.context = parent.getContext();
        return new ViewHolder(vista);
    }

    //Con este metodo vinculamos la vista de cada elemento y damos los valores a cada componente
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int posicion) {
        final Items item = listItems.get(posicion);

        //Se configura el listener para cada elemento para los eventos click y LongClick
        holder.setListener(item, eventoListener);
        //Agregar valores y a los items
        holder.tvDetalleItem.setText(item.getDetalleCompleto());
        holder.tvValorItem.setText(item.getValorDescontadoMostrar());
        holder.tvDescuentoItem.setText(item.getValorDescuentoMostrar());
    }

    @Override
    public int getItemCount() {
        return this.listItems.size();
    }

    public void agregarItem(Items item){
        //if (!listItems.contains(item)) {
            listItems.add(item);
            notifyDataSetChanged();
        //}
    }

    public void clear() {
        listItems.clear();
        notifyDataSetChanged();
    }

    public List<Items> obtenerItemsTotales() {
        return listItems;
    }

    //Clase creada para ViewHolder, extension arriba, ademas se le debe agregar su constructor
    class ViewHolder extends RecyclerView.ViewHolder {

        //Elementos creados con ButterKnife arriba en la vista inflada (Método ViewHolder onCreateViewHolder), a traves de item_list
        @BindView(R.id.imgCart)
        AppCompatImageView imgCart;
        @BindView(R.id.tvDetalleItem)
        AppCompatTextView tvDetalleItem;
        @BindView(R.id.tvValorItem)
        AppCompatTextView tvValorItem;
        @BindView(R.id.tvDescuentoItem)
        AppCompatTextView tvDescuentoItem;
        @BindView(R.id.llValores)
        LinearLayoutCompat llValores;
        @BindView(R.id.containerMain)
        RelativeLayout containerMain;

        //constructor para ViewHolder
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            //Con esta linea vinculamos de forma manual los elementos creados
            ButterKnife.bind(this, itemView);
        }

        //Creación de método auxiliar que permite añadir un listener al ViewHolder para poder enviar el elemento seleccionado a la actividad
        void setListener(final Items items, final OnItemClickListener listener) {
            containerMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(items);
                }
            });
            containerMain.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    listener.onLongItemClick(items);
                    return true;
                }
            });
        }
    }


}
