package com.example.nicolascatalan.calculadescuentos;

import android.content.ClipData;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements OnItemClickListener {

    @BindView(R.id.etPrecio)
    TextInputEditText etPrecio;
    @BindView(R.id.etDescuento)
    TextInputEditText etDescuento;
    @BindView(R.id.etDetalle)
    TextInputEditText etDetalle;
    @BindView(R.id.tvValorTotal)
    AppCompatTextView tvValorTotal;
    @BindView(R.id.tvDescuentoTotal)
    AppCompatTextView tvDescuentoTotal;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private ItemsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        configAdapter();
        configRecyclerView();

        //generarItem();
    }

    private void generarItem() {
        String[] detalle = {"Ejemplo"};
        float[] valor = {25990};
        float[] descuento = {30};
        float[] valorDescontado = {18193};
        float[] valorDescuento = {7797};

        for(int i = 0; i < 1; i++){
            Items item = new Items(i+1, valor[i], descuento[i], detalle[i], valorDescontado[i], valorDescuento[i]);
            adapter.agregarItem(item);
        }
    }

    private void configAdapter() {
        //Para que esta linea no de error debe implementarse el "implements OnItemClickListener" arriba al inicio de la clase, y se les debe implementar sus metodos (onItemClick & onLongItemClick)
        adapter = new ItemsAdapter(new ArrayList<Items>(), this);
    }

    private void configRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @OnClick({R.id.btnLimpiar, R.id.btnAgregar})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLimpiar:
                clearAll();
                break;
            case R.id.btnAgregar:
                calcularDescuento();
                break;
        }
    }

    private void calcularDescuento() {


        if (validarCampos()) {
            final float precioIngresado    = Float.valueOf(etPrecio.getText().toString().trim());
            final float descuentoIngresado = Float.valueOf(etDescuento.getText().toString().trim());

            final float descuentoObtenido = precioIngresado * descuentoIngresado / 100;
            final float valorObtenido     = precioIngresado - descuentoObtenido;

            enviarItem(valorObtenido, descuentoObtenido);
            clearInputs();

            float[] valores = sumaTotales(valorObtenido, descuentoObtenido);
            tvValorTotal.setText(Metodos.formateaValor(valores[0]));
            tvDescuentoTotal.setText(Metodos.formateaValor(valores[1]));

        }
    }

    private float[] sumaTotales(float valorObtenido, float descuentoObtenido) {

        float[] valores = {0, 0};

        List<Items> listItems = new ArrayList<>();
        listItems = adapter.obtenerItemsTotales();

        for (Items listado : listItems){
            valores[0] += listado.getValorDescontado();
            valores[1] += listado.getValorDescuento();
        }

        return valores;
    }

    private void enviarItem(float valorObtenido, float descuentoObtenido) {

        Items item = new Items();
        item.setValor(Float.valueOf(etPrecio.getText().toString().trim()));
        item.setDescuento(Float.valueOf(etDescuento.getText().toString().trim()));
        item.setDetalle(etDetalle.getText().toString().trim().isEmpty() ? "Sin detalles" : etDetalle.getText().toString().trim());
        item.setValorDescontado(valorObtenido);
        item.setValorDescuento(descuentoObtenido);

        adapter.agregarItem(item);
    }

    public void clearInputs() {
        etPrecio.setText("");
        etDescuento.setText("");
        etDetalle.setText("");
        etPrecio.requestFocus();
    }
    public void clearAll(){
        clearInputs();

        adapter.clear();
        tvValorTotal.setText("");
        tvDescuentoTotal.setText("");
    }

    private boolean validarCampos() {
        boolean esValido = true;

        if (etDescuento.getText().toString().trim().isEmpty()){
            etDescuento.setError(getString(R.string.addCalDesc_error_requerido));
            etDescuento.requestFocus();
            esValido = false;
        }
        if (etPrecio.getText().toString().trim().isEmpty()){
            etPrecio.setError(getString(R.string.addCalDesc_error_requerido));
            etPrecio.requestFocus();
            esValido = false;
        }

        return esValido;
    }

    /*****
     * Métodos implementados por la interface OnItemClickListener
     * ****/
    @Override
    public void onItemClick(Items items) {
        //Aqui egregaremos el codigo para dar opciones de edicion
    }

    @Override
    public void onLongItemClick(Items items) {
        //Aqui agregaremos un pequeño vibrate y luego eliminar, ademas debemos ejecutar el metodo que actualiza los valores
    }
}
