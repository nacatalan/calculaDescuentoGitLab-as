package com.example.nicolascatalan.calculadescuentos;

public class Constantes {

    public static final String OPERADOR_DECIMAL = "%.1f";
    public static final String OPERADOR_ENTERO = "%.0f";

}