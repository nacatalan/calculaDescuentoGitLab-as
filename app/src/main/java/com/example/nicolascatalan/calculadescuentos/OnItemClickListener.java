package com.example.nicolascatalan.calculadescuentos;

//Mediante esta interfaz se realiza la comunicacion del adaptador (ItemsAdapter) con la actividad (MainActivity)
//para poder pasar los elementos seleccionados de forma limpia y amigable
public interface OnItemClickListener {
    //Método para pulsacion normal sobre el item
    void onItemClick(Items items);
    //Método para pulsacion larga sobre el item
    void onLongItemClick(Items items);

}
