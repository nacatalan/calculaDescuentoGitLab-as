package com.example.nicolascatalan.calculadescuentos;

import java.util.Objects;

public class Items {

    public static final String ORDEN = "orden";

    private long id;
    private float valor;
    private float descuento;
    private String detalle;
    private float valorDescontado;
    private float valorDescuento;

    public Items() {
    }

    public Items(int id, float valor, float descuento, String detalle, float valorDescontado, float valorDescuento) {
        this.id = id;
        this.valor = valor;
        this.descuento = descuento;
        this.detalle = detalle;
        this.valorDescontado = valorDescontado;
        this.valorDescuento = valorDescuento;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public static String getORDEN() {
        return ORDEN;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public float getDescuento() {
        return descuento;
    }

    public float getValorDescontado() {
        return valorDescontado;
    }

    public void setValorDescontado(float valorDescontado) {
        this.valorDescontado = valorDescontado;
    }

    public void setDescuento(float descuento) {
        this.descuento = descuento;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }


    public float getValorDescuento() {
        return valorDescuento;
    }

    public void setValorDescuento(float valorDescuento) {
        this.valorDescuento = valorDescuento;
    }

    public String getDetalleCompleto() {
        return this.detalle + "\n($ " + Metodos.formateaValor(this.valor) + " | " + Metodos.formateaValor(this.descuento) + "%)";
    }

    public String getValorDescontadoMostrar() {

        return "$ " + Metodos.formateaValor(this.getValorDescontado());
    }

    public String getValorDescuentoMostrar() {
        return "$ " + Metodos.formateaValor(this.getValorDescuento());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Items items = (Items) o;
        return id == items.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

}
